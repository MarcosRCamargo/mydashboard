import { createRouter, createWebHistory } from 'vue-router'

const Home = () => import('../views/Home/Index.vue')
const Feedbacks = () => import('../views/Feedbacks/Index.vue')
const WhatsappMessages = () => import('../views/WhatsappMessages/Index.vue')
const Credentials = () => import('../views/Credentials/Index.vue')
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/feedbacks',
    name: 'Feedbacks',
    component: Feedbacks,
    meta: {
      hasAuth: true
    }
  },
  {
    path: '/whatsapp-send-message',
    name: 'WhastappMessages',
    component: WhatsappMessages
  },
  {
    path: '/credentials',
    name: 'Credentials',
    component: Credentials,
    meta: {
      hasAuth: true
    }
  },
  {
    path: '/.pathMach(.*)*',
    redirect: {
      name: 'Home'
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
