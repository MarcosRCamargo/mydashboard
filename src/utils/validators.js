export function validadeEmptyAndLenth8 (value) {
  if (!value) {
    return '* Este campo é obrigatório!'
  }
  if (value.length < 8) {
    return '* Este campo precis de pelo menos oito caracteres.'
  }
  return true
}
export function validadeEmptyAndLenth3 (value) {
  if (!value) {
    return '* Este campo é obrigatório!'
  }
  if (value.length < 3) {
    return '* Este campo precis de pelo menos três caracteres.'
  }
  return true
}
export function validadeEmptyAndNumber (value) {
  if (!value) {
    return '* Este campo é obrigatório!'
  }
  if (value.length < 9) {
    return '* Este campo precis de pelo menos nove digitos.'
  }
  return true
}
export function validadeEmptyAndEmail (value) {
  if (!value) {
    return '* Este campo é obrigatório!'
  }
  const isValid = /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i.test(value)
  if (!isValid) {
    return '* Informa um e-mail válido.'
  }
  return true
}
