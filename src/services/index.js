import axios from 'axios'
import AuthService from './auth'
import VenonService from './venon'

const API_ENVS = {
  production: '',
  development: '',
  local: 'http://localhost:3000',
  venon: 'http://localhost:8000'
}

const httpClient = axios.create({
  baseURL: API_ENVS.local
})
const venonClient = axios.create({
  baseURL: API_ENVS.venon
})
httpClient.interceptors.response.use((response) => response, (error) => {
  const canThrowAnError = error.request.status === 0 || error.request.status === 500
  if (canThrowAnError) {
    throw new Error(error.message)
  }
  return error
})

export default {
  auth: AuthService(httpClient),
  venon: VenonService(venonClient)
}
