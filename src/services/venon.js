export default venonCLient => ({
  sendMessage: async ({ number, message }) => {
    const response = await venonCLient.post('/send-message', {
      number,
      message
    })
    let errors = null

    if (!response.data) {
      errors = {
        status: response.request.status,
        statusText: response.request.statusText
      }
    }
    return {
      data: response.data,
      errors
    }
  },
  getContacts: async () => {
    const response = await venonCLient.post('/contatcs')
    let errors = null

    if (!response.data) {
      errors = {
        status: response.request.status,
        statusText: response.request.statusText
      }
    }
    return {
      data: response.data,
      errors
    }
  }
})
