export default httpCLient => ({
  login: async ({ email, password }) => {
    const response = await httpCLient.post('/auth/login', {
      email,
      password
    })
    let errors = null

    if (!response.data) {
      errors = {
        status: response.request.status,
        statusText: response.request.statusText
      }
    }
    return {
      data: response.data,
      errors
    }
  }
})
